package mpi.apps.first;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import mpi.apps.first.api.Movie;
import mpi.apps.first.api.MovieApi;
import mpi.apps.first.api.MovieApiInterface;
import mpi.apps.first.api.Result;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static java.lang.Integer.parseInt;

public class ApiMovieActivity extends AppCompatActivity {
    private static final String TAG = "retrofit";
    private int current_page = 1;
    private String current_search = "";
    private String year_search = "----";
    private String filter_search = "----";
    private Boolean adult_switch = false;
    private static int total_pages;
    private static int total_results;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_api_movie);
        handleRecyclerView();
        callApi(); //appel api (movies..) et remplir liste

        ImageView search_menu = findViewById(R.id.search_menu);
        search_menu.setOnClickListener((v)->openSearchFilter());

        /*
        Switch adult_switch_search = this.findViewById(R.id.adult_search_switch);


        adult_switch_search.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    adult_switch = true;
                } else {
                    adult_switch = false;
                }
            }
        });
        */

        ImageView search_reset_menu = findViewById(R.id.search_reset_menu);
        search_reset_menu.setOnClickListener((v)->{
            current_search = "";
            current_page = 1;
            adult_switch = false;
            filter_search = "----";
            year_search = "----";
            callApi();
        });

        Button btnForward = findViewById(R.id.btn_forward);
        btnForward.setOnClickListener((v)->AddPage());

        Button btnForwardTen = findViewById(R.id.btn_forwardTen);
        btnForwardTen.setOnClickListener((v)->AddTenPage());

        Button btnForwardThousand = findViewById(R.id.btn_forwardThousand);
        btnForwardThousand.setOnClickListener((v)->AddThousandPage());

        Button btnBackward = findViewById(R.id.btn_backward);
        btnBackward.setOnClickListener((v)->BackPage());

        Button btnBackwardTen = findViewById(R.id.btn_backwardTen);
        btnBackwardTen.setOnClickListener((v)->BackTenPage());

        Button btnBackwardThousand = findViewById(R.id.btn_backwardThousand);
        btnBackwardThousand.setOnClickListener((v)->BackThousandPage());
    }

    private void openSearchFilter() {
        AlertDialog.Builder builder = new AlertDialog.Builder(new ContextThemeWrapper(this, R.style.AlertDialogCustom));
        builder.setTitle("Search filter");
        final View customLayout = getLayoutInflater().inflate(R.layout.search_filter_layout_movie, null);
        builder.setView(customLayout);

        EditText search_text = customLayout.findViewById(R.id.search_text);

        Spinner spinner = customLayout.findViewById(R.id.spinner_date);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.date_array, android.R.layout.simple_spinner_dropdown_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        spinner.setSelection(adapter.getPosition(year_search));

        Spinner spinner_filter = customLayout.findViewById(R.id.spinner_filter);
        ArrayAdapter<CharSequence> adapter_filter = ArrayAdapter.createFromResource(this, R.array.filter_array, android.R.layout.simple_spinner_dropdown_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_filter.setAdapter(adapter_filter);
        spinner_filter.setSelection(adapter_filter.getPosition(filter_search));

        search_text.setText(current_search);

        builder.setPositiveButton("Search...", (dialog, which) -> {
            String text = search_text.getText().toString();
            current_search = text;
            current_page = 1;
            year_search = spinner.getSelectedItem().toString();
            filter_search = spinner_filter.getSelectedItem().toString();
            callApi(text, year_search);
        });

        builder.setNegativeButton("Close", (dialog, which) -> {
            //fermer boite dialogue
            dialog.dismiss();
        });

        AlertDialog dialog = builder.create();
        dialog.show();

    }

    private void BackPage() {
        if(current_page > 1) current_page -= 1;

        if(!current_search.equals("") || !filter_search.equals("")){
            callApi(current_search, year_search);
        }else{
            callApi();
        }
    }

    private void AddPage() {
        if(current_page+1 <= total_pages) current_page += 1;
        if(!current_search.equals("") || !filter_search.equals("")){
            callApi(current_search, year_search);
        }else{
            callApi();
        }
    }

    private void BackTenPage() {
        if(current_page > 10) current_page -= 10;
        if(!current_search.equals("") || !filter_search.equals("")){
            callApi(current_search, year_search);
        }else{
            callApi();
        }
    }

    private void AddTenPage() {
        if(current_page+10 <= total_pages) current_page += 10;
        if(!current_search.equals("") || !filter_search.equals("")){
            callApi(current_search, year_search);
        }else{
            callApi();
        }
    }


    private void BackThousandPage() {
        if(current_page > 100) current_page -= 100;
        if(!current_search.equals("") || !filter_search.equals("")){
            callApi(current_search, year_search);
        }else{
            callApi();
        }
    }

    private void AddThousandPage() {
        if(current_page+100 <= total_pages) current_page += 100;
        if(!current_search.equals("") || !filter_search.equals("")){
            callApi(current_search, year_search);
        }else{
            callApi();
        }
    }

    private void callApi() {
        MovieApiInterface apiclient = MovieApi.getInstance();

        TextView num_page;
        num_page = this.findViewById(R.id.page_num);

        if(current_page == 1){
            apiclient.getResult().enqueue(new Callback<Result>() {
                @Override
                public void onResponse(Call<Result> call, Response<Result> response) {
                    if(response.isSuccessful()){
                        adapter.removeAllItem();
                        Result result = response.body();
                        total_pages = result.total_pages;
                        total_results = result.total_results;
                        result.movies.forEach(v->adapter.addItem(v));
                        num_page.setText("Page: " + current_page + "/" + total_pages + "     Résultats: " + total_results );

                    }else{
                        //erreur sur réponse
                        Log.i(TAG , "onResponse: "+response.errorBody());
                    }
                }
                @Override
                public void onFailure(Call<Result> call, Throwable t) {
                    //echec appel!
                    Log.i(TAG , "onFailure: "+t.getMessage());
                }
            });
        }
        else {
            apiclient.getResultPage(current_page).enqueue(new Callback<Result>() {
                @Override
                public void onResponse(Call<Result> call, Response<Result> response) {
                    if (response.isSuccessful()) {
                        adapter.removeAllItem();
                        Result result = response.body();
                        total_pages = result.total_pages;
                        total_results = result.total_results;
                        result.movies.forEach(v->adapter.addItem(v));
                        num_page.setText("Page: " + current_page + "/" + total_pages + "     Résultats: " + total_results );

                    } else {
                        //erreur sur réponse
                        Log.i(TAG, "onResponse: " + response.errorBody());
                    }
                }

                @Override
                public void onFailure(Call<Result> call, Throwable t) {
                    //echec appel!
                    Log.i(TAG, "onFailure: " + t.getMessage());
                }
            });
        }
    }

    private void callApi(String current_search, String year_search) {
        MovieApiInterface apiclient = MovieApi.getInstance();
        TextView num_page;
        num_page = this.findViewById(R.id.page_num);

        if(current_page == 1 && year_search.equals("----") && filter_search.equals("----") ){
            apiclient.searchMovie(current_search).enqueue(new Callback<Result>() {
                @Override
                public void onResponse(Call<Result> call, Response<Result> response) {
                    if (response.isSuccessful()) {
                        adapter.removeAllItem();
                        Result result = response.body();
                        total_pages = result.total_pages;
                        total_results = result.total_results;
                        result.movies.forEach(v -> adapter.addItem(v));
                        num_page.setText("Page: " + current_page + "/" + total_pages + "     Résultats: " + total_results );

                    } else {
                        //erreur sur réponse
                        Log.i(TAG, "onResponse: " + response.errorBody());
                    }
                }

                @Override
                public void onFailure(Call<Result> call, Throwable t) {
                    //echec appel!
                    Log.i(TAG, "onFailure: " + t.getMessage());
                }
            });
        }
        else if(current_page != 1 && year_search.equals("----") && filter_search.equals("----")) {
            apiclient.getResultPageSearch(current_page, current_search).enqueue(new Callback<Result>() {
                @Override
                public void onResponse(Call<Result> call, Response<Result> response) {
                    if (response.isSuccessful()) {
                        adapter.removeAllItem();
                        Result result = response.body();
                        total_pages = result.total_pages;
                        total_results = result.total_results;
                        result.movies.forEach(v->adapter.addItem(v));
                        num_page.setText("Page: " + current_page + "/" + total_pages + "     Résultats: " + total_results );

                    } else {
                        //erreur sur réponse
                        Log.i(TAG, "onResponse: " + response.errorBody());
                    }
                }

                @Override
                public void onFailure(Call<Result> call, Throwable t) {
                    //echec appel!
                    Log.i(TAG, "onFailure: " + t.getMessage());
                }
            });
        }
        else if(filter_search.equals("----")){
            apiclient.getResultPageSearchYear(current_search, Integer.parseInt(year_search), current_page).enqueue(new Callback<Result>() {
                @Override
                public void onResponse(Call<Result> call, Response<Result> response) {
                    if (response.isSuccessful()) {
                        adapter.removeAllItem();
                        Result result = response.body();
                        total_pages = result.total_pages;
                        total_results = result.total_results;
                        result.movies.forEach(v->adapter.addItem(v));
                        num_page.setText("Page: " + current_page + "/" + total_pages + "     Résultats: " + total_results);

                    } else {
                        //erreur sur réponse
                        Log.i(TAG, "onResponse: " + response.errorBody());
                    }
                }

                @Override
                public void onFailure(Call<Result> call, Throwable t) {
                    //echec appel!
                    Log.i(TAG, "onFailure: " + t.getMessage());
                }
            });
        }
        else{
            switch(filter_search){
                case "Latest":
                    apiclient.getLatest().enqueue(new Callback<Movie>() {
                        @Override
                        public void onResponse(Call<Movie> call, Response<Movie> response) {
                            if (response.isSuccessful()) {
                                adapter.removeAllItem();
                                Movie movie = response.body();
                                total_pages = 1;
                                total_results = 1;
                                adapter.addItem(movie);
                                num_page.setText("Page: " + current_page + "/" + total_pages + "     Résultats: " + total_results);

                            } else {
                                //erreur sur réponse
                                Log.i(TAG, "onResponse: " + response.errorBody());
                            }
                        }

                        @Override
                        public void onFailure(Call<Movie> call, Throwable t) {
                            //echec appel!
                            Log.i(TAG, "onFailure: " + t.getMessage());
                        }
                    });
                    break;

                case "Now Playing":
                    apiclient.getNowPlaying(current_page).enqueue(new Callback<Result>() {
                        @Override
                        public void onResponse(Call<Result> call, Response<Result> response) {
                            if (response.isSuccessful()) {
                                adapter.removeAllItem();
                                Result result = response.body();
                                total_pages = result.total_pages;
                                total_results = result.total_results;
                                result.movies.forEach(v->adapter.addItem(v));
                                num_page.setText("Page: " + current_page + "/" + total_pages + "     Résultats: " + total_results);

                            } else {
                                //erreur sur réponse
                                Log.i(TAG, "onResponse: " + response.errorBody());
                            }
                        }

                        @Override
                        public void onFailure(Call<Result> call, Throwable t) {
                            //echec appel!
                            Log.i(TAG, "onFailure: " + t.getMessage());
                        }
                    });
                    break;

                case "Popular":
                    apiclient.getPopular(current_page).enqueue(new Callback<Result>() {
                        @Override
                        public void onResponse(Call<Result> call, Response<Result> response) {
                            if (response.isSuccessful()) {
                                adapter.removeAllItem();
                                Result result = response.body();
                                total_pages = result.total_pages;
                                total_results = result.total_results;
                                result.movies.forEach(v->adapter.addItem(v));
                                num_page.setText("Page: " + current_page + "/" + total_pages + "     Résultats: " + total_results);

                            } else {
                                //erreur sur réponse
                                Log.i(TAG, "onResponse: " + response.errorBody());
                            }
                        }

                        @Override
                        public void onFailure(Call<Result> call, Throwable t) {
                            //echec appel!
                            Log.i(TAG, "onFailure: " + t.getMessage());
                        }
                    });
                    break;

                case "Top Rated":
                    apiclient.getTopRated(current_page).enqueue(new Callback<Result>() {
                        @Override
                        public void onResponse(Call<Result> call, Response<Result> response) {
                            if (response.isSuccessful()) {
                                adapter.removeAllItem();
                                Result result = response.body();
                                total_pages = result.total_pages;
                                total_results = result.total_results;
                                result.movies.forEach(v->adapter.addItem(v));
                                num_page.setText("Page: " + current_page + "/" + total_pages + "     Résultats: " + total_results);

                            } else {
                                //erreur sur réponse
                                Log.i(TAG, "onResponse: " + response.errorBody());
                            }
                        }

                        @Override
                        public void onFailure(Call<Result> call, Throwable t) {
                            //echec appel!
                            Log.i(TAG, "onFailure: " + t.getMessage());
                        }
                    });
                    break;

                case "Upcoming":
                    apiclient.getUpcoming(current_page).enqueue(new Callback<Result>() {
                        @Override
                        public void onResponse(Call<Result> call, Response<Result> response) {
                            if (response.isSuccessful()) {
                                adapter.removeAllItem();
                                Result result = response.body();
                                total_pages = result.total_pages;
                                total_results = result.total_results;
                                result.movies.forEach(v->adapter.addItem(v));
                                num_page.setText("Page: " + current_page + "/" + total_pages + "     Résultats: " + total_results);

                            } else {
                                //erreur sur réponse
                                Log.i(TAG, "onResponse: " + response.errorBody());
                            }
                        }

                        @Override
                        public void onFailure(Call<Result> call, Throwable t) {
                            //echec appel!
                            Log.i(TAG, "onFailure: " + t.getMessage());
                        }
                    });
                    break;
            }
        }
    }

    private void OnApiResponse(){

    }
    RecyclerView list;
    mpi.apps.first.ApiMovieActivity.MoviesListAdapter adapter;


    private void handleRecyclerView() {
        list = findViewById(R.id.list);
        List<Movie> data = new ArrayList<>();
        adapter = new mpi.apps.first.ApiMovieActivity.MoviesListAdapter();
        list.setAdapter(adapter);
        list.setLayoutManager(new LinearLayoutManager(this));
        LayoutAnimationController animation =  AnimationUtils.loadLayoutAnimation(mpi.apps.first.ApiMovieActivity.this, R.anim.list_layout_anim);
        list.setLayoutAnimation(animation);
    }
    class MoviesListAdapter extends RecyclerView.Adapter<mpi.apps.first.ApiMovieActivity.MoviesListAdapter.MyViewHolder>{
        List<Movie> data = new ArrayList();

        @Override
        public mpi.apps.first.ApiMovieActivity.MoviesListAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View item_view =  LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_layout_movie, parent, false);
            return new mpi.apps.first.ApiMovieActivity.MoviesListAdapter.MyViewHolder(item_view);
        }

        @Override
        public void onBindViewHolder(MyViewHolder holder, int position) {
            Movie movie = data.get(position);
            holder.title.setText(movie.getTitle());
            holder.id_movie.setText(movie.getId()+"");
            holder.desc.setText(movie.getRelease_date());
            holder.vote.setText(movie.getVote_average() + "/10");

            Picasso.get()
                    .load("https://image.tmdb.org/t/p/w200" + movie.getPoster_path())
                    .resize(200,300)
                    .into(holder.img);
        }
        @Override
        public int getItemCount() {
            return this.data.size();
        }
        public void addItem(Movie movie) {
            this.data.add(movie);
            this.notifyDataSetChanged();
        }
        public void removeAllItem() {
            this.data.removeAll(data);
            this.notifyDataSetChanged();
        }
        class MyViewHolder extends RecyclerView.ViewHolder{
            ImageView img;
            TextView title, desc, id_movie, vote;
            public MyViewHolder(View itemView) {
                super(itemView);
                img = itemView.findViewById(R.id.img);
                title = itemView.findViewById(R.id.nom);
                desc = itemView.findViewById(R.id.desc);
                vote = itemView.findViewById(R.id.vote);
                id_movie = itemView.findViewById(R.id.id_movie);
                itemView.setOnClickListener((v)->displayMore((String) id_movie.getText()));
            }
        }

    }

    private void displayMore(String id) {

        MovieApiInterface apiclient = MovieApi.getInstance();
        apiclient.getMovie(id).enqueue(new Callback<Movie>() {
            @Override
            public void onResponse(Call<Movie> call, Response<Movie> response) {
                if(response.isSuccessful()){
                    //reponse ok!
                    Movie movie = response.body();
                    show_dialog(movie);

                }else{
                    //erreur sur réponse
                    Log.i(TAG , "onResponse: "+response.errorBody());
                }
            }

            @Override
            public void onFailure(Call<Movie> call, Throwable t) {
                Log.i(TAG , "onFailure: "+t.getMessage());
            }
        });
    }

    private void show_dialog(Movie movie) {

        AlertDialog.Builder builder = new AlertDialog.Builder(new ContextThemeWrapper(this, R.style.AlertDialogCustom));
        builder.setTitle("Display More");
        final View customLayout = getLayoutInflater().inflate(R.layout.custom_display_more_layout_movie, null);
        builder.setView(customLayout);

        TextView titre = customLayout.findViewById(R.id.edtTitle);
        TextView overview = customLayout.findViewById(R.id.edtOverview);
        TextView original_language = customLayout.findViewById(R.id.edtOriginal_language);
        TextView release_date = customLayout.findViewById(R.id.edtRelease_date);
        TextView vote_average = customLayout.findViewById(R.id.edtVote_average);

        ImageView poster_film = customLayout.findViewById(R.id.poster_film);


        titre.setText(movie.getTitle());
        overview.setText(movie.getOverview());
        original_language.setText(movie.getOriginal_language());
        release_date.setText(movie.getRelease_date());
        vote_average.setText(movie.getVote_average() + "/10");
        Picasso.get()
                .load("https://image.tmdb.org/t/p/w500" + movie.getPoster_path())
                .resize(500,780)
                .into(poster_film);

        builder.setPositiveButton("Learn more", (dialog, which) -> {
            Uri uri = Uri.parse("https://www.themoviedb.org/movie/" + movie.getId());
            startActivity(new Intent(Intent.ACTION_VIEW, uri));
        });

        builder.setNegativeButton("Close", (dialog, which) -> {
            //fermer boite dialogue
            dialog.dismiss();
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    public class SpinnerActivity extends Activity implements AdapterView.OnItemSelectedListener {

        public void onItemSelected(AdapterView<?> parent, View view,
                                   int pos, long id) {
            // An item was selected. You can retrieve the selected item using
            // parent.getItemAtPosition(pos)

            String item = (parent.getItemAtPosition(pos)).toString();
           Log.i(TAG, item);
        }

        public void onNothingSelected(AdapterView<?> parent) {
            // Another interface callback
        }
    }
}

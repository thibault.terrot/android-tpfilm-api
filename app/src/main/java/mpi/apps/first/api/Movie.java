package mpi.apps.first.api;

import android.net.Uri;

import com.google.gson.annotations.SerializedName;

import com.google.gson.annotations.SerializedName;
import java.io.Serializable;
public class Movie implements Serializable {
    @SerializedName("id")
    public int id;
    @SerializedName("adult")
    public boolean adult;
    @SerializedName("original_title")
    public String original_title;
    @SerializedName("title")
    public String title;
    @SerializedName("release_date")
    public String release_date;
    @SerializedName("original_language")
    public String original_language;
    @SerializedName("overview")
    public String overview;
    @SerializedName("backdrop_path")
    public String backdrop_path;
    @SerializedName("poster_path")
    public String poster_path;
    @SerializedName("vote_average")
    public String vote_average;

    public String getVote_average() {
        return vote_average;
    }

    public void setVote_average(String vote_average) {
        this.vote_average = vote_average;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isAdult() {
        return adult;
    }

    public void setAdult(boolean adult) {
        this.adult = adult;
    }

    public String getOriginal_title() {
        return original_title;
    }

    public void setOriginal_title(String original_title) {
        this.original_title = original_title;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getRelease_date() {
        return release_date;
    }

    public void setRelease_date(String release_date) {
        this.release_date = release_date;
    }

    public String getOriginal_language() {
        return original_language;
    }

    public void setOriginal_language(String original_language) {
        this.original_language = original_language;
    }

    public String getOverview() {
        return overview;
    }

    public void setOverview(String overview) {
        this.overview = overview;
    }

    public String getBackdrop_path() {
        return backdrop_path;
    }

    public void setBackdrop_path(String backdrop_path) {
        this.backdrop_path = backdrop_path;
    }

    public String getPoster_path() {
        return poster_path;
    }

    public void setPoster_path(String poster_path) {
        this.poster_path = poster_path;
    }

    //    "genre_ids":[28,12,14,35],
    //    "popularity":3692.281,
    //    "video":false,
    //    "vote_average":8,
    //    "vote_count":3201
    @Override
    public String toString() {
        return "[" + id + "] " + title +  " | " + release_date;
    }
}

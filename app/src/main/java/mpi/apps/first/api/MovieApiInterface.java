package mpi.apps.first.api;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface MovieApiInterface {
    String API_KEY = "?api_key=7e40b802e1714f6ecd10f6443643b3d8";


    @GET("discover/movie"+API_KEY)
    Call<Result> getResult();

    @GET("discover/movie" + API_KEY)
    Call<Result> getResultPage(@Query("page") int page);

    @GET("movie/{movie}" + API_KEY)
    Call<Movie> getMovie(@Path("movie") String movie);

    @GET("search/movie" + API_KEY)
    Call<Result> searchMovie(@Query("query") String query);

    @GET("search/movie" + API_KEY)
    Call<Result> getResultPageSearch(@Query("page") int page,
                                    @Query("query") String query);

    @GET("search/multi" + API_KEY)
    Call<Result> getResultPageSearchYear(@Query("query") String query,
                                         @Query("primary_release_year") int year,
                                         @Query("page") int page);

    @GET("movie/latest" + API_KEY)
    Call<Movie> getLatest();

    @GET("movie/now_playing" + API_KEY)
    Call<Result> getNowPlaying(@Query("page") int page);

    @GET("movie/popular" + API_KEY)
    Call<Result> getPopular(@Query("page") int page);

    @GET("movie/top_rated" + API_KEY)
    Call<Result> getTopRated(@Query("page") int page);

    @GET("movie/upcoming" + API_KEY)
    Call<Result> getUpcoming(@Query("page") int page);
}

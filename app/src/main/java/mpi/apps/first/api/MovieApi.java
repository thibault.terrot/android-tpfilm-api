package mpi.apps.first.api;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MovieApi {
    private static final String BASE_URL = "https://api.themoviedb.org/3/";
    private static final String TAG = "retrofit";
    public static MovieApiInterface getInstance() {
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
        MovieApiInterface apiInterface = retrofit.create(MovieApiInterface.class);
        return apiInterface;
    }
}

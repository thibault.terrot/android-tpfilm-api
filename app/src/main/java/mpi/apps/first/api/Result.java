package mpi.apps.first.api;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Result {
    @SerializedName("page")
    public String page;
    @SerializedName("results")
    public List<Movie> movies;
    @SerializedName("total_pages")
    public int total_pages;

    @SerializedName("total_results")
    public int total_results;
    @Override
    public String toString() {
        return "Result{" +
                "page='" + page + '\'' +
                ", results=" + movies +
                '}';
    }
}
